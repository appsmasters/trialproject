require(['/static/js/rfc822-validate.js'], function(validation) {
    var email = $('#email');
    var submit = $('#send-button');

    function isValid(field, valid)
    {
        if(valid) {
            field.prev('p.error').text('');
            field.removeClass('not-valid');
            submit.prop('disabled', false);
        }
        else {
            field.prev('p.error').text('Provided email address is not valid!');
            field.addClass('not-valid');
            submit.prop('disabled', true);
        }
    }

    var validate = function() {
        var emailValue = $(this).val();
        var validationResult = validation(emailValue);

        isValid($(this), validationResult);
    }

    email.change(validate).keyup(validate);
});
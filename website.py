import tornado.web
import tornado.template
import tornado.ioloop
import tornado_mysql
from tornado import gen
import bcrypt
from validate_email import validate_email
from settings import *


class IntegrityException(Exception):
    def __init__(self, column_name):
        self.column_name = column_name


class NoRowInsertedException(Exception):
    pass


TEMPLATE_LOADER = tornado.template.Loader("templates/")

''' possible validation status: used to generate panel in Bootstrap template '''
STATUS = {
    'INCORRECT_NAME': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Incorrect name. This field cannot be empty and should have at most 32 characters.'
    },
    'INCORRECT_EMAIL_LENGTH': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Incorrect email. The field length should be between 3 and 128 characters.'
    },
    'INCORRECT_EMAIL': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Incorrect email address.'
    },
    'INCORRECT_PASSWORD': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Incorrect password. Minimum length is 8 characters, at least one capital letter and one digit.'
    },
    'USER_OK': {
         'panel_class': 'panel-success',
         'panel_heading': 'Success',
         'panel_body': 'Information successfully stored.'
    },
    'NOT_UNIQUE_EMAIL': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Integrity error. Email address exists in database.'
    },
    'NOT_UNIQUE_NAME': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'Integrity error. Name exists in database.'
    },
    'MYSQL_ERROR': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'MySQL connection error. Could not connect.'
    },
    'NO_INSERT': {
        'panel_class': 'panel-danger',
        'panel_heading': 'Error',
        'panel_body': 'No row inserted to database.'
    }
}


@gen.coroutine
def integrity_check_name(connection, name):
    cursor = connection.cursor()
    query = 'SELECT COUNT(*) FROM users where name = %s;'

    yield cursor.execute(query, (name,))
    row = cursor.fetchone()
    cursor.close()

    if row[0] > 0:
        raise IntegrityException('name')


@gen.coroutine
def integrity_check_email(connection, email):
    cursor = connection.cursor()
    query = 'SELECT COUNT(*) FROM users where email = %s;'

    yield cursor.execute(query, (email,))
    row = cursor.fetchone()
    cursor.close()

    if row[0] > 0:
        raise IntegrityException('email')


@gen.coroutine
def integrity_check(connection, name, email):
    """ checks if such name or email exists in database """

    yield integrity_check_name(connection, name)
    yield integrity_check_email(connection, email)


@gen.coroutine
def save_user(connection, name, email, hashed_password):
    """ saving user to database """
    yield integrity_check(connection, name, email)

    cursor = connection.cursor()
    query = 'INSERT INTO users (name, email, password) VALUES (%s, %s, %s);'

    yield cursor.execute(query, (name, email, hashed_password,))

    rowcount = cursor.rowcount

    yield connection.commit()

    cursor.close()

    if rowcount != 1:
        raise NoRowInsertedException()


def validate_password(password):
    """ password validation according to the three rules """

    length_rule = len(password) >= 8
    upper_alpha_rule = len(list(filter(lambda character: character.isupper(), password))) > 0
    digit_rule = len(list(filter(lambda character: character.isdigit(), password))) > 0

    return length_rule and upper_alpha_rule and digit_rule


def validate_name(name):
    return 1 <= len(name) <= 32


def validate_email_len(email):
    return 3 <= len(email) <= 128


class UserRegistration(tornado.web.RequestHandler):
    @gen.coroutine
    def prepare(self):
        try:
            self._connection = yield tornado_mysql.connect(
                host=MYSQL_HOST,
                port=3306,
                user=MYSQL_USER,
                password=MYSQL_PASSWORD,
                db=MYSQL_DATABASE
            )
        except tornado_mysql.err.OperationalError:
            self._connection = None

    @gen.coroutine
    def get(self):
        status_cookie = self.get_cookie('status', None)
        self.clear_cookie('status')

        is_connection = self._connection is not None

        if is_connection:
            status = STATUS.get(status_cookie, None)
        else:
            status = STATUS.get('MYSQL_ERROR', None)

        self.write(TEMPLATE_LOADER.load("registration_form.html").generate(
            status=status,
            is_connection=is_connection
        ))

    def on_finish(self):
        if self._connection is not None:
            self._connection.close()

    def _validate_request(self):
        """ request data validation called before creating user """

        name = self.get_argument('name').encode('utf8')
        email = self.get_argument('email')
        password = self.get_argument('password')

        if not validate_name(name):
            self.set_cookie('status', 'INCORRECT_NAME')
            return False

        if not validate_email_len(email):
            self.set_cookie('status', 'INCORRECT_EMAIL_LENGTH')
            return False

        if not validate_email(email):
            self.set_cookie('status', 'INCORRECT_EMAIL')
            return False

        if not validate_password(password):
            self.set_cookie('status', 'INCORRECT_PASSWORD')
            return False

        hashed_password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())

        return name, email, hashed_password

    @gen.coroutine
    def post(self):
        if self._connection is not None:
            validation_result = self._validate_request()

            if validation_result != False:
                try:
                    yield tornado.gen.Task(save_user, self._connection, *validation_result)
                    self.set_cookie('status', 'USER_OK')
                except NoRowInsertedException:
                    self.set_cookie('status', 'NO_INSERT')
                except IntegrityException as e:
                    if e.column_name == 'email':
                        self.set_cookie('status', 'NOT_UNIQUE_EMAIL')
                    elif e.column_name == 'name':
                        self.set_cookie('status', 'NOT_UNIQUE_NAME')

        self.redirect('/')


if __name__ == '__main__':
    web_application = tornado.web.Application([
        (r"/", UserRegistration),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {'path': 'static/'}),
    ])  # TODO: SSL

    web_application.listen(8000)

    tornado.ioloop.IOLoop.current().start()
